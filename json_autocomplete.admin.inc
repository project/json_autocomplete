<?php

/**
 * @file
 * Settings form.
 */

/**
 * Implements hook_settings_form().
 */
function json_autocomplete_settings_form($form) {
  $form = array();
  $stream_wrappers = json_autocomplete_get_available_stream_wrapper();
  $form['json_autocomplete_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Json autocomplete configurations'),
  );
  $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_SELECTED_FILE_SYSTEM] = array(
    '#type' => 'radios',
    '#title' => t('JSON File Location'),
    '#description' => t('Please select location on which you want to save json file.'),
    '#options' => $stream_wrappers,
    '#required' => TRUE,
  );
  if (!empty($default_value = json_autocomplete_get_default_file_system())) {
    $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_SELECTED_FILE_SYSTEM]['#default_value'] = $default_value;
  }
  $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_NUMBER_OF_RESULTS] = array(
    '#type' => 'textfield',
    '#title' => t('Number of result to be show on autocomplete'),
    '#default_value' => variable_get(JSON_AUTOCOMPLETE_NUMBER_OF_RESULTS, 25),
    '#element_validate' => array('_element_validate_integer_positive'),
    '#required' => TRUE,
  );
  $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_UPDATE_ON_INSERT] = array(
    '#type' => 'checkbox',
    '#title' => t('Update json file on insert'),
    '#default_value' => variable_get(JSON_AUTOCOMPLETE_UPDATE_ON_INSERT, 0),
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
    '#description' => t('If you uncheck it file will not be updated on insert, but will update on cron.'),
  );
  $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_UPDATE_ON_UPDATE] = array(
    '#type' => 'checkbox',
    '#title' => t('Update json file on update'),
    '#default_value' => variable_get(JSON_AUTOCOMPLETE_UPDATE_ON_UPDATE, 0),
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
    '#description' => t('If you uncheck it file will not be updated on update, but will update on cron.'),
  );
  $form['json_autocomplete_fieldset'][JSON_AUTOCOMPLETE_UPDATE_ON_DELETE] = array(
    '#type' => 'checkbox',
    '#title' => t('Update json file on delete'),
    '#default_value' => variable_get(JSON_AUTOCOMPLETE_UPDATE_ON_DELETE, 0),
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
    '#description' => t('If you uncheck it file will not be updated on delete, but will update on cron.'),
  );
  $form['#submit'][] = 'json_autocomplete_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Submit callback of admin setting form.
 */
function json_autocomplete_settings_form_submit($form, $form_state) {
  if (trim($form_state['values'][JSON_AUTOCOMPLETE_SELECTED_FILE_SYSTEM]) != trim(json_autocomplete_get_default_file_system())) {
    $result = db_select('json_autocomplete_fields')->fields('json_autocomplete_fields', array('fid'))->execute()->fetchAll();
    if (count($result)) {
      foreach ($result as $file) {
        $file_load = file_load($file->fid);
        if ($file_load->fid) {
          file_move($file_load, $form_state['values'][JSON_AUTOCOMPLETE_SELECTED_FILE_SYSTEM] . '://', FILE_EXISTS_REPLACE);
        }
      }
    }
  }
}

/**
 * Menu callback for field status report.
 *
 * @return string
 *   Table formatted output in html.
 */
function json_autocomplete_field_status_report() {
  $header = array(
    'S.N.',
    'Field Name',
    'Field Type',
    'File Exist',
    'File Name',
    'Status',
    'Rebuild',
  );
  $rows = array();
  $query = db_select('json_autocomplete_fields');
  $query->fields('json_autocomplete_fields', array(
    'field_name',
    'field_type',
    'fid',
    'status',
  ));
  $result = $query->execute()->fetchAll();
  if (count($result)) {
    $i = 1;
    foreach ($result as $row) {
      $file = file_load($row->fid);
      $exist = file_exists($file->uri) ? 'Yes' : 'No';
      $status = ($row->status || !file_exists($file->uri)) ? 'Changed (Rebuild to Update)' : 'Updated';
      $rows[] = array(
        $i,
        $row->field_name,
        $row->field_type,
        $exist,
        $file->filename,
        $status,
        l(t('Rebuild now'), 'admin/config/json-autocomplete/rebuild/' . $row->field_name),
      );
    }
  }
  $field_report['json_autocomplete'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $field_report;
}

/**
 * Menu callback for json rebuild for a particular  field.
 *
 * @param string $field_name
 *   Field name.
 */
function json_autocomplete_rebuild_json_field($field_name) {
  json_autocomplete_json_rebuild($field_name);
  drupal_set_message(t('File rebuid successfully.'));
  drupal_goto(JSON_AUTOCOMPLETE_FIELD_LOG_REPORT);
}
