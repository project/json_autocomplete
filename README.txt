CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Manish Kumar <manishupdh900@gmail.com>

This module can be used for enhanced autocomplete widget, as we know drupal
autocomplete behaves very slow with increase of data in database, so we used
json string instead of database searching and search being performed from json,
it gives better performance compare to default autocomplete widget.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
Administrator can administrate this module
URL : <yourdomain.com>/admin/config/json-autocomplete/config
