<?php

/**
 * @file
 * Json autocomplete Drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function json_autocomplete_drush_command() {
  $items['rebuild-json-autocomplete'] = array(
    'callback' => 'json_autocomplete_rebuild_json_autocomplete',
    'description' => 'Rebuild JSON files for field which uses json_autocomplete.',
    'aliases' => array('rja'),
    'examples' => array(
      'Rebuild all json files' => 'drush rebuild-json-autocomplete',
      'reebuild only one field for e.g. field_tags' => 'drush rebuild-json-autocomplete field_tags',
      'rebuild multiple json files for fields e.g. field_tags, field_taxonomy' => 'drush rebuild-json-autocomplete field_tags field_taxonomy',
    ),
  );
  return $items;
}

/**
 * Callback for the rebuild-json-autocomplete command.
 */
function json_autocomplete_rebuild_json_autocomplete() {
  $args = func_get_args();
  if (!count($args)) {
    json_autocomplete_json_rebuild();
  }
  else {
    foreach ($args as $field_name) {
      json_autocomplete_json_rebuild($field_name);
    }
  }
}
