<?php

/**
 * @file
 * Module helper functions.
 */

/**
 * Our custom validate on widget selection form.
 */
function json_autocomplete_field_ui_widget_type_form_validate($form, &$form_state) {
  if ($form_state['values']['widget_type'] == JSON_AUTOCOMPLETE_WIDGET_TAXONOMY_TERM_REFERNCE && empty(json_autocomplete_get_default_file_system())) {
    form_set_error('widget_type', t('Please select JSON File Location from !json_autocomplete', array('!json_autocomplete' => l(t('json autocomplete settings page.'), 'admin/config/json-autocomplete/config'))));
  }
}

/**
 * Our custom submit on widget selection form to save json data into file.
 */
function json_autocomplete_field_ui_widget_type_form_submit($form, &$form_state) {
  if ($form_state['values']['widget_type'] == JSON_AUTOCOMPLETE_WIDGET_TAXONOMY_TERM_REFERNCE) {
    $widget_type = field_info_widget_types($form_state['values']['widget_type']);
    $response = json_autocomplete_save_file($widget_type['field types'][0], $form['#field_name']);
    if ($response['status']) {
      json_autocomplete_update_field_log('insert', $widget_type['field types'][0], $form['#field_name'], $response['file']->fid);
    }
  }
  elseif ($form['basic']['widget_type']['#default_value'] == JSON_AUTOCOMPLETE_WIDGET_TAXONOMY_TERM_REFERNCE) {
    json_autocomplete_update_field_log('delete', JSON_AUTOCOMPLETE_WIDGET_TAXONOMY_TERM_REFERNCE, $form['#field_name']);
  }
}

/**
 * Gets and save json data to a file.
 *
 * @param string $widget_type
 *   Type of widget.
 * @param string $field_name
 *   Field machine name.
 */
function json_autocomplete_save_file($widget_type, $field_name, $cron = FALSE) {
  $data = json_autocomplete_get_json_data($widget_type, $field_name);
  $json_file_name = $field_name . '_json_autocomplete.json';
  if (!empty($file_location = json_autocomplete_get_default_file_system())) {
    $file = json_autocomplete_file_save_data($data, $file_location . '://' . $json_file_name, FILE_EXISTS_REPLACE, $cron);
    return array('status' => TRUE, 'file' => $file);
  }
  else {
    form_set_error(NULL, t('Please select JSON file location from admin settings.'));
    return array('status' => FALSE);
  }
}

/**
 * Gets json encoded data for a field from database.
 *
 * @param string $widget_type
 *   Type of widget.
 * @param string $field_name
 *   Field machine name.
 *
 * @return string
 *   Json encoded string.
 */
function json_autocomplete_get_json_data($widget_type, $field_name) {
  switch ($widget_type) {
    case 'taxonomy_term_reference':
      return json_encode(json_autocomplete_get_taxonomy_term_reference_data($field_name));
  }
}

/**
 * Gets json encoded data for taxonomy_term_reference field type.
 *
 * @param string $field_name
 *   Field machine name.
 *
 * @return array
 *   Array of all terms.
 */
function json_autocomplete_get_taxonomy_term_reference_data($field_name) {
  $field = field_info_field($field_name);
  $vids = array();
  $vocabularies = taxonomy_vocabulary_get_names();
  foreach ($field['settings']['allowed_values'] as $tree) {
    $vids[] = $vocabularies[$tree['vocabulary']]->vid;
  }
  $query = db_select('taxonomy_term_data', 't');
  $query->addTag('translatable');
  $query->addTag('term_access');
  return $query->fields('t', array('tid', 'name'))->condition('t.vid', $vids)->execute()->fetchAllKeyed();
}

/**
 * Page callback: Outputs JSON for taxonomy autocomplete suggestions.
 *
 * Path: json/autocomplete.
 *
 * @param string $autocomplete_type
 *   Auto complete type.
 * @param string $field_name
 *   Field machine name.
 * @param string $tags_typed
 *   Keyword that already typed.
 */
function json_autocomplete_callback($autocomplete_type, $field_name = '', $search_type = 1, $tags_typed = '') {
  $matches = array();
  $args = func_get_args();
  // Shift off the $autocomplete_type & $field_name argument.
  $args = array_slice($args, 3);
  $tags_typed = implode('/', $args);
  // The user enters a comma-separated list of tags. We only use the last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));
  switch ($autocomplete_type) {
    case 'taxonomy':
      $wrapper = file_stream_wrapper_get_instance_by_uri(json_autocomplete_get_default_file_system() . '://' . $field_name . '_json_autocomplete.json');
      $path = $wrapper->realpath();
      // Decode the JSON into an associative array.
      $json = json_decode(file_get_contents($path), TRUE);
      $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';
      if ($tag_last != '') {
        foreach ($json as $value) {
          $escaped_value = trim(trim($value, "'"), '"');
          if ((int) $search_type) {
            if (drupal_strtolower(drupal_substr($escaped_value, 0, drupal_strlen($tag_last))) === $tag_last && !in_array($escaped_value, $tags_typed)) {
              $n = $value;
              // Terms containing commas or quotes must be wrapped in quotes.
              if (strpos($value, ',') !== FALSE || strpos($value, '"') !== FALSE) {
                $n = '"' . str_replace('"', '""', $value) . '"';
              }
              $matches[$prefix . $n] = $value;
              if (count($matches) == variable_get(JSON_AUTOCOMPLETE_NUMBER_OF_RESULTS, 25)) {
                break;
              }
            }
          }
          else {
            if (strpos(drupal_strtolower($escaped_value), drupal_strtolower($tag_last)) !== FALSE && !in_array($escaped_value, $tags_typed)) {
              $n = $value;
              // Terms containing commas or quotes must be wrapped in quotes.
              if (strpos($value, ',') !== FALSE || strpos($value, '"') !== FALSE) {
                $n = '"' . str_replace('"', '""', $value) . '"';
              }
              $matches[$prefix . $n] = $value;
              if (count($matches) == variable_get(JSON_AUTOCOMPLETE_NUMBER_OF_RESULTS, 25)) {
                break;
              }
            }
          }
        }
      }
      break;
  }
  drupal_json_output($matches);
}
